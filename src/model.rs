use crate::app::{ModelOptions, ModelState};
use std::time;
use std::time::Instant;

pub struct Model {
    options: ModelOptions,
    time: f64,
    state: Option<ModelState>,
}

impl Model {
    pub fn new(options: ModelOptions) -> Self {
        Model {
            options,
            time: 0.0,
            state: None,
        }
    }

    fn calculate(&self) -> ModelState {
        let (m1, m2, l) = (self.options.m1, self.options.m2, self.options.l);
        let g = 9.8;
        let omega = ((g / l) * (m1 + m2) / m1).sqrt();
        let amplitude = 3.14 / 4.0;

        let angle = amplitude * (omega * self.time).cos();
        let x = -(m2 * l) / (m1 + m2) * angle;

        ModelState { angle, x }
    }

    pub fn update(&mut self, t: f64) {
        self.time = t;
        self.state = None
    }

    pub fn state(&mut self) -> &ModelState {
        self.state.get_or_insert(self.calculate())
    }
}
