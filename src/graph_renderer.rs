use crate::data_pool::DataPool;
use plotters::prelude::*;
use plotters::prelude::*;
use plotters_canvas::CanvasBackend;
use web_sys::HtmlCanvasElement;

// TODO: create an appropriate error type
pub fn draw(
    data_pool: &DataPool<f64>,
    el: HtmlCanvasElement,
    labels: &(String, String),
) {
    let backend = CanvasBackend::with_canvas_object(el)
        .expect("unable to retrieve canvas context");

    let root = backend.into_drawing_area();

    // if data_pool.data.is_empty() {
    //     return
    // }

    let range = data_pool.range;
    let first = data_pool.data.first().unwrap_or(&(0.0, 0.0)).0;
    let last = first + range;

    root.fill(&WHITE);
    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(50)
        .y_label_area_size(50)
        .margin(23)
        .build_cartesian_2d(first as f32..last as f32, -1.2f32..1.2f32)
        .expect("");

    chart
        .configure_mesh()
        .y_desc(&labels.0)
        .x_desc(&labels.1)
        .axis_desc_style(("sans-serif", 15))
        .draw();

    chart
        .draw_series(LineSeries::new(
            data_pool.data.iter().map(|(t, x)| (*t as f32, *x as f32)),
            &RED,
        ))
        .unwrap()
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    root.present();
}
