use yew::prelude::*;
use yew::services;
use yew::services::ConsoleService;

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct TabButtonProps {
    pub name: String,

    #[prop_or(false)]
    pub active: bool,
    pub callback: Callback<u32>,
    pub number: u32,
}

pub struct TabButtonComponent {
    props: TabButtonProps,
    link: ComponentLink<Self>,
}

pub enum Msg {
    OnClick,
}

impl Component for TabButtonComponent {
    type Message = Msg;
    type Properties = TabButtonProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        TabButtonComponent { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::OnClick => {
                self.props.callback.emit(self.props.number);
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let class = if self.props.active {
            "nav-link active"
        } else {
            "nav-link"
        };
        html! {
        <button class=class type="button" role="tab" onclick={self.link.callback(|_| Msg::OnClick )}>{&self.props.name}</button>
        }
    }
}
