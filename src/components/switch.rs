use yew::prelude::*;

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct SwitchProps {
    #[prop_or_default]
    pub title: String,
    pub on_press: Callback<()>,
    #[prop_or(false)]
    pub checked: bool,
}

pub struct SwitchComponent {
    link: ComponentLink<Self>,
    title: String,
    on_press: Callback<()>,
    checked: bool,
}

pub enum Msg {
    Clicked,
}

impl Component for SwitchComponent {
    type Message = Msg;
    type Properties = SwitchProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        SwitchComponent {
            link,
            title: props.title,
            on_press: props.on_press,
            checked: props.checked,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Clicked => {
                self.on_press.emit(());
                self.checked = !self.checked;
            }
        }
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="form-check form-switch">
                    <input onclick={ self.link.callback(|_| Msg::Clicked) }
                    class="form-check-input" type="checkbox" checked={ self.checked } />
                <label class="form-check-label" >{ &self.title }</label>
            </div>
        }
    }
}
