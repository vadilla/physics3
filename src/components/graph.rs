use crate::data_pool::DataPool;
use crate::graph_renderer;
use std::future::Future;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::HtmlCanvasElement;
use yew::prelude::*;
use yew::services::ConsoleService;

pub struct Graph {
    canvas_ref: NodeRef,
    data: DataPool<f64>,
    link: ComponentLink<Self>,
    props: GraphProps,
}

#[derive(Properties, PartialEq, Clone, Debug)]
pub struct GraphProps {
    pub data_pool: DataPool<f64>,
    pub labels: (String, String),
}

impl Graph {
    fn redraw_canvas(&self) {
        let canvas_opt: Option<HtmlCanvasElement> = self.canvas_ref.cast();
        if let Some(canvas) = canvas_opt {
            graph_renderer::draw(
                &self.props.data_pool,
                canvas,
                &self.props.labels,
            );
        }
    }
}

impl Component for Graph {
    type Message = ();
    type Properties = GraphProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Graph {
            canvas_ref: NodeRef::default(),
            data: DataPool::new(10.0),
            link,
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.redraw_canvas();
        if self.props != props {
            self.props = props;
            self.redraw_canvas();
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <div class="row">
                <canvas ref=self.canvas_ref.clone() style="min-height: 365px; width: 100%" />
            </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        ConsoleService::log("mmad");
        if first_render {
            let canvas_opt: Option<HtmlCanvasElement> = self.canvas_ref.cast();
            if let Some(canvas) = canvas_opt {
                let rect = canvas.get_bounding_client_rect();
                let (w, h) = (rect.width(), rect.height());
                canvas.set_width(w as u32);
                canvas.set_height(h as u32);

            }

            self.redraw_canvas();
        }
    }
}
