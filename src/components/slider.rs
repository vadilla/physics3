use yew::prelude::*;

pub struct SliderComponent {
    link: ComponentLink<SliderComponent>,
    on_change: Callback<i32>,
    title: String,
    range: (i32, i32),
    value: f64,
}

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct SliderProps {
    pub on_change: Callback<i32>,
    pub title: String,
    pub range: (i32, i32),
    pub value: f64,
}

pub enum Msg {
    Change(String),
}

impl Component for SliderComponent {
    type Message = Msg;
    type Properties = SliderProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        SliderComponent {
            link,
            title: props.title,
            on_change: props.on_change,
            range: props.range,
            value: props.value,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Change(value) => {
                let value = value.parse().expect("expected number");
                self.on_change.emit(value);
                self.value = value as f64;
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let label = format!("{} {} см", self.title, self.value);

        html! {
        <>
            <label class="form-label">{ label }</label>
            <input oninput={ self.link.callback(|e: InputData| Msg::Change(e.value)) }
            type="range" class="form-range" min={ self.range.0 } max={ self.range.1 } value={ self.value } />
        </>
        }
    }
}
