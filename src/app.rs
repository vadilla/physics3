use yew::prelude::*;

use super::components::button::ButtonComponent;
use super::components::canvas::CanvasComponent;
use super::components::collapse::CollapseComponent;
use super::components::graph::Graph;
use super::components::input::InputComponent;
use super::components::slider::SliderComponent;
use super::components::switch::SwitchComponent;
use super::components::tab::TabComponent;
use super::components::tab_button::TabButtonComponent;
use super::model::Model;

use crate::data_pool::DataPool;
use wasm_bindgen::__rt::std::time::{Duration, Instant};
use yew::services::interval::IntervalTask;
use yew::services::render::RenderTask;
use yew::services::{ConsoleService, IntervalService, RenderService};

pub struct App {
    link: ComponentLink<Self>,
    time: f64,
    last_time: f64,
    show_graphs: bool,
    model: Model,
    model_state: ModelState,
    model_options: ModelOptions,
    _render_loop: Option<RenderTask>,
    _task: Option<IntervalTask>,
    angle_pool: DataPool<f64>,
    position_pool: DataPool<f64>,
    trace_pool: DataPool<ModelState>,
    active_tab: u32,
}

pub enum AppMessage {
    StartClick,
    PauseClick,
    SwitchGraphs,
    Mass1Change(i32),
    Mass2Change(i32),
    LengthChange(i32),
    Tick(f64),
    ActivateTab(u32),
}

#[derive(PartialEq, Clone, Debug)]
pub struct ModelState {
    pub angle: f64,
    pub x: f64,
}

#[derive(PartialEq, Clone, Debug)]
pub struct ModelOptions {
    pub m1: f64,
    pub m2: f64,
    pub l: f64,
}

impl Component for App {
    type Message = AppMessage;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let default_options = ModelOptions {
            m1: 1.0,
            m2: 3.0,
            l: 1.5,
        };
        let mut model = Model::new(default_options.clone());
        App {
            link,
            time: 0.0,
            show_graphs: false,
            model_state: model.state().clone(),
            model,
            model_options: default_options,
            _task: None,
            _render_loop: None,
            last_time: 0.0,
            angle_pool: DataPool::new(10.0),
            trace_pool: DataPool::new(1.0),
            position_pool: DataPool::new(10.0),
            active_tab: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            AppMessage::StartClick => {
                let render_frame = self.link.callback(AppMessage::Tick);
                let handle =
                    RenderService::request_animation_frame(render_frame);
                self._render_loop = Some(handle);

                self.model = Model::new(self.model_options.clone());
            }
            AppMessage::PauseClick => {
                self._render_loop = None;
                self.last_time = 0.0;
            }
            AppMessage::Mass1Change(x) => {
                self.model_options.m1 = x as f64;
                self.model = Model::new(self.model_options.clone());
            }
            AppMessage::Mass2Change(x) => {
                self.model_options.m2 = x as f64;
                self.model = Model::new(self.model_options.clone());
            }
            AppMessage::LengthChange(x) => {
                self.model_options.l = (x as f64) / 100.0;
                self.model = Model::new(self.model_options.clone());
            }
            AppMessage::SwitchGraphs => {
                self.show_graphs = !self.show_graphs;
            }
            AppMessage::Tick(t) => {
                if self.last_time == 0.0 {
                    self.last_time = t / 1000.0;
                }
                self.time += t / 1000.0 - self.last_time;
                self.last_time = t / 1000.0;
                self.model.update(self.time as f64);
                self.model_state = self.model.state().clone();
                self.trace_pool.append(self.time, self.model_state.clone());
                self.angle_pool.append(self.time, self.model_state.angle);
                self.position_pool.append(self.time, self.model_state.x);

                let render_frame = self.link.callback(AppMessage::Tick);
                let handle =
                    RenderService::request_animation_frame(render_frame);

                // A reference to the new handle must be retained for the next render to run.
                self._render_loop = Some(handle);
            }
            AppMessage::ActivateTab(x) => {
                self.active_tab = x;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let on_change_mass1 =
            self.link.callback(|x| AppMessage::Mass1Change(x));
        let on_change_mass2 =
            self.link.callback(|x| AppMessage::Mass2Change(x));
        let on_change_length =
            self.link.callback(|x| AppMessage::LengthChange(x));
        let on_click_switch = self.link.callback(|_| AppMessage::SwitchGraphs);
        let on_click_start = self.link.callback(|_| AppMessage::StartClick);
        let on_click_pause = self.link.callback(|_| AppMessage::PauseClick);
        let on_tab = self.link.callback(|x| AppMessage::ActivateTab(x));

        html! {
                    <>
                    <div class="container h-auto">
            <div class="row">
                <div class="col">
                    <div class="card d-flex flex-column mb-3" style="min-height: 100%">
                        <div class="row no-gutters flex-fill" style="min-height: 400px">
                            <div class="col-md-4">
                                <div class="card-body">
                                    // <ButtonComponent title={"Старт"} on_press={on_click_modal} kind="btn-secondary" />
                                    <InputComponent title={"Масса ползуна"}
                                    on_change={ on_change_mass1 } measure="кг" value={ self.model_options.m1 } />
                                    <InputComponent title={"Масса маятника"}
                                    on_change={ on_change_mass2 } measure="кг" value={ self.model_options.m2 } />
                                    <SliderComponent title={"Длина стержня"}
                                    on_change={ on_change_length } range={(1,400)} value={ self.model_options.l * 100.0 }/>
                                    <SwitchComponent title={"Показать графики"} on_press={ on_click_switch } checked={ self.show_graphs } />
                                    <div class="mt-3">
                                    <ButtonComponent title={"Старт"} on_press={ on_click_start }/>
                                    <ButtonComponent title={"Пауза"} on_press={ on_click_pause }/>
                                    </div>
                                    <p class="mt-2 mb-2 text-muted">
                                    { format!("t = {:.3} с", self.time) }
                                    </p>
                                    <p class="text-muted">
                                    { format!("g = {} м/с²", 9.8) }
                                    </p>

                                </div>
                            </div>
                            <CanvasComponent model_state={ &self.model_state }
                            model_options={ &self.model_options } trace_pool={ &self.trace_pool }/>
                        </div>
                        { if self.show_graphs { html! {
                        <div class="card-footer no-gutters flex-fill">
                            <div class="col">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <TabButtonComponent name="Угол отклонения" number=0  callback={on_tab.clone()}
                                    active={ if self.active_tab == 0 { true } else { false } }/>
                                    <TabButtonComponent name="Смещение" number=1  callback={on_tab}
                                    active={ if self.active_tab == 1 { true } else { false } }/>
                                </div>
                                <div class="tab-content" id="myTabContent">
                                    {if self.active_tab == 0 {
                                        html! {
                                        <TabComponent name="График отклонения маятника" id="lol" active=true>
                                            <Graph data_pool={ &self.angle_pool } labels=("Угол отклонения α °".into(), "Время T c".into())/>
                                        </TabComponent>
                                        }
                                        }
                                        else {
                                            html! {
                                            <TabComponent name="График сдвига ползуна" id="kek" active=true>
                                        <Graph data_pool={ &self.position_pool } labels=("Значения сдвига x м".into(), "Время T c".into())/>
                                    </TabComponent>
                                            }
                                        }
                                    }


                                </div>
                            </div>
                        </div>
                        }}
                        else {html!{}}}
                    </div>
                </div>
            </div>
        </div>
                    </>
                }
    }
}
