use yew::services::ConsoleService;

#[derive(Clone, PartialEq, Debug)]
pub struct DataPool<T> {
    pub data: Vec<(f64, T)>,
    pub range: f64,
}

impl<T> DataPool<T> {
    pub fn new(range: f64) -> Self {
        DataPool {
            data: vec![],
            range,
        }
    }

    pub fn append(&mut self, t: f64, value: T) {
        self.data.push((t, value));

        let first_t = t - self.range;

        while !self.data.is_empty()
            && self.data.first().map_or(0.0, |x| x.0) < first_t
        {
            self.data.drain(0..1);
        }

        // ConsoleService::log(&*format!("{:?}", self.data.len()))
    }
}
