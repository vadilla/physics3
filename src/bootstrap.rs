use wasm_bindgen::prelude::*;
use web_sys::Element;

#[wasm_bindgen(module = "bootstrap")]
extern "C" {
    #[wasm_bindgen]
    pub type Collapse;

    #[wasm_bindgen(constructor)]
    pub fn new(e: web_sys::Element) -> Collapse;

    #[wasm_bindgen(method)]
    pub fn show(this: &Collapse);

    #[wasm_bindgen(method)]
    pub fn hide(this: &Collapse);
}

#[wasm_bindgen(module = "bootstrap")]
extern "C" {
    #[wasm_bindgen]
    pub type Tab;

    #[wasm_bindgen(constructor)]
    pub fn new(e: web_sys::Element) -> Tab;

    #[wasm_bindgen(method)]
    pub fn show(this: &Tab);
}

fn get_element_by_id(id: &str) -> Element {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    document
        .get_element_by_id(id)
        .expect(&format!("no element with id '{}'", id))
}

pub fn get_collapse_by_id(id: &str) -> Collapse {
    let element = get_element_by_id(id);

    Collapse::new(element)
}

pub fn get_tab_by_id(id: &str) -> Tab {
    let element = get_element_by_id(id);

    Tab::new(element)
}
